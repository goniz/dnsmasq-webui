#!/usr/bin/python2

from flask.ext.script import Manager, Shell, Server
from flask_assets import ManageAssets
from webapp import app, assets_env

if '__main__' == __name__:
	app.debug = True
	manager = Manager(app)
	manager.add_command("runserver", Server(host='0.0.0.0'))
	manager.add_command("shell", Shell())
	manager.add_command("assets", ManageAssets(assets_env))
	manager.run()
