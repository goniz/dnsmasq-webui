#!/usr/bin/python2

from flask import render_template, request
from webapp import app
from utils import set_class_active

@app.before_request
def reload():
	if request.path.startswith('/static/'):
		return
	app.leasesfile.reload()
	app.configfile.reload()

@app.route('/')
def index():
	leases = app.leasesfile.entries
	return render_template('index.html', set_class_active=set_class_active, leases=leases)

@app.route('/hostnames')
def hostnames():
	return render_template('hostnames.html', set_class_active=set_class_active, hostnames=app.configfile.dns_hostnames())

@app.route('/reserved')
def reserved():
	return render_template('reserved.html', set_class_active=set_class_active, reserved=app.configfile.reserved_addresses())

@app.route('/keyvals')
def keyvals():
	return render_template('keyvals.html', set_class_active=set_class_active, keyvals=app.configfile.keyvals())

@app.route('/unknowns')
def unknowns():
	return render_template('unknowns.html', set_class_active=set_class_active, values=app.configfile.unknown_values())

@app.route('/booleans')
def booleans():
	return render_template('booleans.html', set_class_active=set_class_active, values=app.configfile.booleans())

@app.route('/settings')
def settings():
	return render_template('layout.html')
