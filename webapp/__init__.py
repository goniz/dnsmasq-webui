#!/usr/bin/python2

from flask import Flask
from flask_assets import Environment
from webassets.loaders import PythonLoader as PythonAssetsLoader

from dnsmasq_utils.leasesfile import DnsmasqLeases
from dnsmasq_utils.configfile import DnsmasqConfig

app = Flask(__name__)
app.leasesfile = DnsmasqLeases('/var/lib/misc/dnsmasq.leases')
app.configfile = DnsmasqConfig('/etc/dnsmasq.conf')

from webapp import views
import assets

assets_env = Environment(app)
assets_loader = PythonAssetsLoader(assets)
for name, bundle in assets_loader.load_bundles().iteritems():
	assets_env.register(name, bundle)
