#!/usr/bin/python2

from flask import url_for

def set_class_active(request, view):
	if request.path == url_for(view):
		return 'class=active'
	return ''
