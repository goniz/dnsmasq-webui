#!/usr/bin/python2

from flask_assets import Bundle

common_css = Bundle('vendor/bootstrap-3.1.1-dist/css/bootstrap.css',
					'css/dashboard.css',
					output='public/css/common.css')

common_js = Bundle('vendor/jquery/jquery-1.11.1.min.js',
					'vendor/bootstrap-3.1.1-dist/js/bootstrap.min.js',
					'js/docs.min.js',
					output='public/js/common.js')


